const print = require('./utils/print');
const asyncForEach = require('./utils/asyncForEach');

module.exports = {
  print,
  asyncForEach
}
