const print = function(options, text) {
  if(options.hasOwnProperty('fg') &&
     fgColors.hasOwnProperty(options.fg)
  ) {
    return console.log(fgColors[options.fg], text)
  }

  if(options.hasOwnProperty('bg') &&
     bgColors.hasOwnProperty(options.bg)
  ) {
    return console.log(bgColors[options.bg], text);
  }
  
  return console.log(fgColors.white, text);
}

const fgColors = {
  black : "\x1b[30m",
  red : "\x1b[31m",
  green : "\x1b[32m",
  yellow : "\x1b[33m",
  blue : "\x1b[34m",
  magenta : "\x1b[35m",
  cyan: "\x1b[36m",
  white: "\x1b[37m"
}

const bgColors = {
  black : "\x1b[40m",
  red : "\x1b[41m",
  green : "\x1b[42m",
  yellow : "\x1b[43m",
  blue : "\x1b[44m",
  magenta : "\x1b[45m",
  cyan : "\x1b[46m",
  white : "\x1b[47m",
}

module.exports = print;
